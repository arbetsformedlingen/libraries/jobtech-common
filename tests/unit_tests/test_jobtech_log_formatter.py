import logging
from unittest.mock import patch

from jobtech.common.customlogging import JobtechLogFormatter


@patch("os.linesep", "\n")
def test_jobtech_log_formatter_constructor_sets_development_attributes():
    formatter = JobtechLogFormatter(is_develop_mode=True)

    assert formatter.is_develop_mode
    assert formatter.linesep == "\n"


def test_jobtech_log_formatter_constructor_sets_production_attributes():
    formatter = JobtechLogFormatter(is_develop_mode=False)

    assert not formatter.is_develop_mode
    assert formatter.linesep == "\r"


def test_jobtech_log_formatter_lastreplace():
    replaced_string = JobtechLogFormatter.lastreplace("ab\ncd\n", "\n", "\r")

    assert replaced_string == "ab\ncd\r"


def test_jobtech_log_formatter_formats_string():
    record = logging.makeLogRecord({"msg": "some-log-message"})

    formatter = JobtechLogFormatter(is_develop_mode=True)

    message = formatter.format(record)

    assert message == "some-log-message"


def test_jobtech_log_formatter_formats_multiline_string():
    record = logging.makeLogRecord({"msg": "some\nlog\nmessage\n"})

    formatter = JobtechLogFormatter(is_develop_mode=False)

    message = formatter.format(record)

    assert message == "some\rlog\rmessage\n"


def test_jobtech_log_formatter_formats_json():
    json = """{
    "jsontestprop1": "jsontestval1",
    "jsontestprop2": "jsontestval2"

}"""

    record = logging.makeLogRecord({"msg": json})

    formatter = JobtechLogFormatter(is_develop_mode=False)

    message = formatter.format(record)

    assert (
        message
        == '{"jsontestprop1": "jsontestval1", "jsontestprop2": "jsontestval2"}\n'
    )


def test_jobtech_log_formatter_formats_string_with_curly_braces():
    not_json = """function test{
    alert('just testing logging...');
}"""
    record = logging.makeLogRecord({"msg": not_json})

    formatter = JobtechLogFormatter(is_develop_mode=False)

    message = formatter.format(record)

    assert message == "function test{\r    alert('just testing logging...');\r}\n"


def test_jobtech_log_formatter_formats_object():
    obj = {"foo": "bar", "inner": {"level2": "cool"}}
    record = logging.makeLogRecord({"msg": obj})

    formatter = JobtechLogFormatter(is_develop_mode=False)

    message = formatter.format(record)

    assert message == "{'foo': 'bar', 'inner': {'level2': 'cool'}}\n"


# Is this meaningful?
def test_jobtech_log_formatter_formats_exception():
    exception = Exception("Something blew up!")
    record = logging.makeLogRecord({"msg": exception})

    formatter = JobtechLogFormatter(is_develop_mode=False)

    message = formatter.format(record)

    assert message == "Something blew up!\n"


def test_jobtech_log_formatter_formatMessage():
    record = logging.makeLogRecord(
        {
            "asctime": "some-time",
            "levelname": "some-level",
            "name": "some-name",
            "message": "some-log-message\n",
        }
    )

    formatter = JobtechLogFormatter(
        "%(asctime)s|%(levelname)s|%(name)s|MESSAGE: %(message)s", is_develop_mode=False
    )

    message = formatter.formatMessage(record)

    assert message == "some-time|some-level|some-name|MESSAGE: some-log-message\r"


def test_jobtech_log_formatter_formatException():
    formatter = JobtechLogFormatter(
        "%(asctime)s|%(levelname)s|%(name)s|MESSAGE: %(message)s", is_develop_mode=False
    )

    exception = ValueError("Test\nwith\nmultiple\nlines")

    message = formatter.formatException(
        (type(exception), exception, exception.__traceback__)
    )

    assert message == "ValueError: Test\rwith\rmultiple\rlines"


def test_jobtech_log_formatter_formatException_with_stack_trace():
    formatter = JobtechLogFormatter(
        "%(asctime)s|%(levelname)s|%(name)s|MESSAGE: %(message)s", is_develop_mode=False
    )

    def function_that_raises_exception():
        raise Exception("This is an exception.")

    message = None

    try:
        function_that_raises_exception()
    except Exception as error:
        message = formatter.formatException((type(error), error, error.__traceback__))

    assert message.count("\n") <= 1
    assert message.count("\r") > 3
    assert len(message) > 100
    assert "This is an exception." in message
    assert message.startswith("Traceback")
