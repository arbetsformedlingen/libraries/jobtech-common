from unittest.mock import Mock, patch

from jobtech.common.customlogging import create_log_formatter


@patch("os.getenv", Mock(return_value="development"))
def test_create_log_formatter_in_development():
    formatter = create_log_formatter()

    assert formatter.is_develop_mode
    assert formatter._fmt == "%(asctime)s|%(levelname)s|%(name)s|MESSAGE: %(message)s"
    assert formatter.datefmt == "%Y-%m-%d %H:%M:%S%z"


@patch("os.getenv", Mock(return_value="production"))
def test_create_log_formatter_in_production():
    formatter = create_log_formatter()

    assert not formatter.is_develop_mode
    assert formatter._fmt == "%(asctime)s|%(levelname)s|%(name)s|MESSAGE: %(message)s"
    assert formatter.datefmt == "%Y-%m-%d %H:%M:%S%z"
