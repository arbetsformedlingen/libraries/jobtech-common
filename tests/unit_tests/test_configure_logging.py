import logging
from unittest.mock import patch

from jobtech.common.customlogging import configure_logging


@patch("jobtech.common.customlogging.set_custom_log_level")
def test_configure_logging(set_custom_log_level_mock):
    configure_logging()

    root = logging.getLogger()
    handlers = root.handlers

    assert len(handlers) == 1
    assert root.level == 10  # DEBUG level

    formatter = handlers[0].formatter

    assert "JobtechLogFormatter" in str(type(formatter))

    set_custom_log_level_mock.assert_called_once_with([])


@patch("jobtech.common.customlogging.set_custom_log_level")
def test_configure_logging_sets_up_local_modules(set_custom_log_level_mock):
    configure_logging(local_modules=["mock_module"])

    root = logging.getLogger()
    handlers = root.handlers

    assert len(handlers) == 1
    assert root.level == 10  # DEBUG level

    formatter = handlers[0].formatter

    assert "JobtechLogFormatter" in str(type(formatter))

    set_custom_log_level_mock.assert_called_once_with(["mock_module"])
