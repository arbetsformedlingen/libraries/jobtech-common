import logging
from unittest.mock import Mock, patch

import pytest

from jobtech.common.customlogging import set_custom_log_level


@pytest.mark.parametrize(
    "level, expected",
    [
        ("debug", logging.DEBUG),
        ("info", logging.INFO),
        ("warning", logging.WARNING),
        ("error", logging.ERROR),
        ("eRroR", logging.ERROR),
    ],
)
@patch("os.getenv")
def test_set_custom_log_level_on_loggers(getenv_mock, level, expected):
    getenv_mock.return_value = level
    test_logger_local_mock = Mock(**{"name": "test.local.logger"})
    test_logger_global_mock = Mock(**{"name": "test.global.logger"})
    test_logger_jobtech_mock = Mock(**{"name": "jobtech"})

    with patch(
        "logging.Logger.manager.loggerDict",
        {
            "test.local.logger": test_logger_local_mock,
            "test.global.logger": test_logger_global_mock,
            "jobtech": test_logger_jobtech_mock,
        },
    ):
        set_custom_log_level(["test.local.logger"])

    test_logger_local_mock.setLevel.assert_called_once_with(expected)
    test_logger_global_mock.setLevel.assert_called_once_with(logging.WARNING)
    test_logger_jobtech_mock.setLevel.assert_called_once_with(expected)
