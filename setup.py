from setuptools import find_packages, setup

setup(
    name="jobtech-common",
    version="2.0.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
)
