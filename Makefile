.ONESHELL:

build:
	rm -rf venv venv3.10
	python3 -m venv venv3.10
	. venv3.10/bin/activate
	python3 -m pip install --upgrade setuptools wheel pip build
	python3 -m build --outdir /tmp/jjec
